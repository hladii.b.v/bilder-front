import React from "react";
import s from "./Tariffs.module.css";
import FixedWrapper from "../../wrappers/FixedWrapper/FixedWrapper";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import TariffCard from "../../misc/TariffCardPage/TariffCardPage";


const Tariffs = (content) => {

    const {
        page_content: { home: homeContent },
      } = content.content;
    
      const { tariffs = [] } = homeContent;
  
    return (
        <div className={s.container}>
            <div className={s.section}>
                <h2 className={s.section__title}>{homeContent.tariffs_title}</h2>
                <FixedWrapper className={s.cards__container}>
                {[...Array(3)].map(
                    (_, i) =>
                    tariffs[i + 1] && (
                        <TariffCard
                        isPrimary={i === 1}
                        tariff={tariffs[i + 1] || {}}
                        key={`tariff__card${i}`}                        
                        month={homeContent.month_1}
                        month_6 ={homeContent.month_6}
                        month_12={homeContent.month_12}
                        selectText={homeContent.select}
                        />
                    )
                )}
                </FixedWrapper>
            </div>
        </div>
  );
};

const mapStateToProps = (state) => ({  
  content: state.content,
});

export default withRouter(connect(mapStateToProps, )(Tariffs));