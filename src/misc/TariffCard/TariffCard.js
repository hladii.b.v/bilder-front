import React from "react";
import s from "./TariffCard.module.css";
import classnames from "classnames";
import Button from "../Button/Button";
import CardWrapper from "../../wrappers/CardWrapper/CardWrapper";
import { Link } from "react-router-dom";

const TariffCard = ({ isPrimary, tariff, selectText, priceText }) => {
  const { tariff_price: tariffPrices, tariff_text: tariffTexts } = tariff;
  return (
    <CardWrapper className={s.container}>
      <div>
        <div
          className={classnames(s.header, { [s.header__primary]: isPrimary })}
        >
          <h4 className={s.title}>{tariff.name}</h4>
          <h3 className={s.price}>
            {tariffPrices[0].value} {priceText}
          </h3>
        </div>
        <div className={s.main__content}>
          {tariffTexts.slice(0, 6).map((text, i) => (
            <div className={s.desc} key={`tariff_text${i}`}>
              <p>{text.value}</p>
            </div>
          ))} 
        </div>
      </div>
      <div className={s.button__container}>
        <Link to="/tariffs">
          <Button className={s.button} title={selectText} />
        </Link>
      </div>
    </CardWrapper>
  );
};

export default TariffCard;
