import React, { useState } from "react";
import s from "./ResetPassword.module.css";
import { Formik, withFormik } from "formik";
import { connect } from "react-redux";
import AuthWrapper from "../../wrappers/AuthWrapper/AuthWrapper";
import Input from "../../misc/Input/Input";
import Button from "../../misc/Button/Button";
import { object, string } from "yup";
import { showModalAction } from "../../store/actions/assetsActions";
import { sendResetCode } from "../../store/api/api";

const ResetPassword = ({
  handleChange,
  handleSubmit,
  handleBlur,
  values,
  errors,
  touched,
  content,
}) => {

  const {
    page_content: { resetPassword:  resetContent}
   
  } = content;
  return (
    <AuthWrapper title={resetContent.title}>
      <div>
        <Input
          label="E-mail"
          name="email"
          value={values.email}
          isError={errors.email}
          onBlur={handleBlur}
          onChange={handleChange}
          placeholder="johndoe@gmail.com"
          containerClass={s.input__container}
        />
        <div className={s.actions__container}>
          <Button
            title={resetContent.reset_button}
            onClick={handleSubmit}
            isDisabled={!touched.email || errors.email}
          />
        </div>
      </div>
    </AuthWrapper>
  );
};

const formikHOC = withFormik({
  mapPropsToValues: () => ({
    email: "",
  }),
  validationSchema: object().shape({
    email: string().email().required(),
  }),
  handleSubmit: (values, { props: { showModal }, resetForm, resetContent }) => {
    sendResetCode(values.email)
      .then(() => {
        showModal(
          resetContent.success,
          resetContent.successMassege
        );
        resetForm({ email: "" });
      })
      .catch(() => {
        showModal(
          resetContent.error,
          resetContent.errorMassege
        );
      });
  },
})(ResetPassword);

const mapStateToProps = (state) => ({
  content: state.content,
});
const mapDispatchToProps = (dispatch) => ({  
  showModal: (title, desc) => dispatch(showModalAction(title, desc)),
});

export default connect(mapStateToProps, mapDispatchToProps)(formikHOC);
