import React from "react";
import s from "./About.module.css";
import { withFormik } from "formik";
import { connect } from "react-redux";
import FixedWrapper from "../../wrappers/FixedWrapper/FixedWrapper";
import Input from "../../misc/Input/Input";
import { object, string } from "yup";
import Button from "../../misc/Button/Button";
import { withRouter } from "react-router";

const About = ({
  handleChange,
  handleBlur,
  handleSubmit,
  errors,
  touched,
  values,
  content,
}) => {

  const {
    page_content: { about:aboutContent },   
  } = content;

  return (
    <FixedWrapper className={s.container}>
      <div className={s.about__container}>
        <h1 className={s.about__title}>{aboutContent.header}</h1>
        <div className={s.about__content}>
          <p>
            <strong>
              {aboutContent.text_1}
            </strong>
          </p>
          <p>
          {aboutContent.text_2}
          </p>
          <p>
          {aboutContent.text_3}
          </p>         
          <p>
          {aboutContent.text_4}
          </p>
          <p>
          {aboutContent.text_5}
          </p>
          <p>
          {aboutContent.text_6}
          </p>
          <p>
          {aboutContent.text_7}            
          </p>
          <h4>{aboutContent.header_1}</h4>
          <h5>{aboutContent.header_1_1}</h5>
          <p>
          {aboutContent.text_8}
          </p>
          <h5>{aboutContent.header_1_2}</h5>
          <p>
          {aboutContent.text_9}
          </p>
        </div>
      </div>
      <div className={s.form}>
        <div className={s.form__inner}>
          <h2 className={s.form__title}>{aboutContent.form_title}</h2>
          <Input
            value={values.email}
            name="email"
            onChange={handleChange}
            onBlur={handleBlur}
            label="E-mail"
            placeholder="johndoe@gmail.com"
            isError={touched.email && errors.email}
            containerClass={s.input__container}
          />
          <Input
            isTextarea
            value={values.message}
            label={aboutContent.form_massege}
            placeholder={aboutContent.form_placeholder}
            onChange={handleChange}
            onBlur={handleBlur}
            name="message"
            isError={touched.message && errors.message}
            containerClass={s.input__container}
          />
          <Button
            isDisabled={
              Object.keys(errors).length || Object.keys(touched).length < 2
            }
            onClick={handleSubmit}
            title={aboutContent.form_button}
          />
        </div>
      </div>
    </FixedWrapper>
  );
};

const formikHOC = withFormik({
  mapPropsToValues: ({ user }) => ({
    email: user.email,
    message: "",
  }),
  validationSchema: object().shape({
    message: string().required(),
    email: string().email().required(),
  }),
  handleSubmit: (values) => {},
})(About);

const mapStateToProps = (state) => ({
  user: state.user,
  content: state.content,
});

export default connect(mapStateToProps, null)(formikHOC);
