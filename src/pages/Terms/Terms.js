import React from "react";
import s from "./Terms.module.css";
import FixedWrapper from "../../wrappers/FixedWrapper/FixedWrapper";
import { connect } from "react-redux";
import { withRouter } from "react-router";

const Terms = (content) => {

  const {
    page_content: { terms: termsContent },   
  } = content.content;

  return (
    <FixedWrapper className={s.container}>
      <h1>{termsContent.header}</h1>
      <p>
        {termsContent.text_1}
      </p>
      <p>
        {" "}
        {termsContent.text_2}
      </p>
      <p>
      {termsContent.text_3}
      </p>{" "}
      <p>
      {termsContent.text_4}
      </p>
      <p>
      {termsContent.text_5}
      </p>
      <p>
      {termsContent.text_6}
      </p>
      <p>
      {termsContent.text_7}
      </p>
    </FixedWrapper>
  );
};

const mapStateToProps = (state) => ({  
  content: state.content,
});

export default withRouter(connect(mapStateToProps, )(Terms));
