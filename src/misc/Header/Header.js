/* eslint-disable no-nested-ternary */
import React, { useEffect, useMemo, useState } from "react";
import s from "./Header.module.css";
import Button from "../Button/Button";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { CSSTransition } from "react-transition-group";
import { ReactComponent as FaTimes } from "../../assets/times.svg";
import { ReactComponent as FaBars } from "../../assets/bars.svg";
import { stack as Menu } from "react-burger-menu";
import { withRouter } from "react-router";
import { changeLanguageAction } from "../../store/actions/contentActions";
import classnames from "classnames";
import Select from "../Select/Select";

const Header = ({ user, history, content, changeLanguage }) => {
  const [isBarOpen, setBarOpen] = useState(null);
  const [isAnimation, setAnimation] = useState(false);
  const [sidebarIcon, setSidebarIcon] = useState(false);
  const [isFirstLoad, setFirstLoad] = useState(false);
  const [isHomeScreen, setHomeScreen] = useState(true);

  const {
    page_content: { home: homeContent,  menu: menuContent },
   
  } = content;



  const { lang, allLanguages } = content;

  const languageOptions = useMemo(() => {
    return allLanguages.map((lang) => ({ label: lang, value: lang }));
  }, [allLanguages]);

  const openSidebar = () => setBarOpen(true);
  const closeSidebar = () => setBarOpen(false);
  const onStateMenuChange = (state) => setBarOpen(state.isOpen);

  const onLanguageSelect = ({ value }) => {
    changeLanguage(value);
  };

  const { pathname } = history.location;

  useEffect(() => {
    if (typeof isBarOpen !== "boolean") return;
    setAnimation((prev) => !prev);
    setTimeout(() => {
      setSidebarIcon((prev) => !prev);
    }, 250);
  }, [isBarOpen]);

  useEffect(() => {
    window.scroll({ left: 0, top: 0 });
    if (pathname === "/") {
      setHomeScreen(true);
    } else if (pathname !== "/" && isHomeScreen) {
      setHomeScreen(false);
    }
    if (!isFirstLoad) {
      setFirstLoad(true);
      return;
    }
    if (isBarOpen) {
      setBarOpen(false);
    }
  }, [pathname]);

  const [isMenuMainisible] = useMemo(() => {
    return [
      !pathname?.includes("/create-site") &&
        !pathname?.includes("/edit-site") &&
        !pathname?.includes("/sites") &&
        !pathname?.includes("/site") &&
        !pathname?.includes("/gallery") &&
        !pathname?.includes("/profile") &&
        !pathname?.includes("/select-template")  
    ];
  }, [pathname]);

  const [isUserMainisible] = useMemo(() => {
    return [
      pathname?.includes("/create-site") ||
        pathname?.includes("/edit-site") ||
        pathname?.includes("/sites") ||
        pathname?.includes("/site") ||
        pathname?.includes("/gallery") ||
        pathname?.includes("/profile") ||
        pathname?.includes("/select-template")          
    ];
  }, [pathname]);

  const [isCreateButtonVisible] = useMemo(() => {
    return [
      !pathname?.includes("/create-site") &&
        !pathname?.includes("/edit-site") &&
        !pathname?.includes("/select-template")  
    ];
  }, [pathname]);

  return (
    <>
      <div>
        <div
          className={classnames(s.header__container, s.desktop__header, {
            [s.header__small]: !isHomeScreen,
          })}
        >
          <div className={s.container}>
            <div className={s.left__container}>
              <Link to="/">
                <img
                  src={require("../../assets/logo.png")}
                  className={s.logo}
                  alt="loading"
                />
              </Link>
              <div className={s.main__content}>
                {isMenuMainisible && (
                  <Link className={s.header__item} to="/">
                     { menuContent.home} 
                  </Link>
                )}
                {isMenuMainisible && (
                  <Link className={s.header__item} to="/about-us">
                   { menuContent.about} 
                  </Link>
                )}
                 {isMenuMainisible && (
                  <Link className={s.header__item} to="/tariffs">
                   { menuContent.tariffs} 
                  </Link>
                )}        
                {isUserMainisible && !!user.id && (
                    <Link to="/gallery" className={s.header__item}>
                      { menuContent.gallery} 
                    </Link>
                  )}
                {isUserMainisible && !!user.id && (
                  <Link to="/sites" className={s.header__item}>
                    { menuContent.sites} 
                  </Link>
                )}
                { isUserMainisible && !!user.id && (
                  <Link to="/profile" className={s.header__item}>
                    { menuContent.profile} 
                  </Link>
                )}
              </div>
              
                <Select
                  options={languageOptions}
                  containerClass={s.language__select}
                  value={lang}
                  onSelect={onLanguageSelect}
                  noDefaultValue
                  valueContainerClass={s.language__value__container}
                  menuClass={s.language__select__menu}
                />
            </div>
            <div className={s.action__container}>
              {user.id ? (
                isMenuMainisible ? (
                  <Link to="/sites" className={s.header__item}>
                    { menuContent.dashboard} 
                  </Link>
                ) : ''
              ) : (
                <Link to="/login" className={s.header__item}>
                  {menuContent.login} 
                </Link>
              )}
              {isCreateButtonVisible && (
                <Link to="/select-template">
                  <Button
                    size="lg"
                    title= {menuContent.create} 
                    className={s.create__btn}
                  />
                </Link>
              )}
            </div>
            
          </div>
          {isHomeScreen && (
            <div className={s.home__header__inner}>
              <img
                src={require("../../assets/home-header.png")}
                alt="loading"
                className={s.home__header__image}
              />
              <div className={s.home__header__main__content}>
                <h1 className={s.home__header__title}>
                    {homeContent.header_1}
                </h1>
                <p className={s.home__header__desc}>
                    {homeContent.header_text_1}
                </p>
              </div>
            </div>
          )}
        </div>
      </div>
      <div className={`${s.header__container} ${s.mobile__header__container}`}>
        <div className={s.mobile__header}>
          <CSSTransition
            in={isAnimation}
            timeout={500}
            classNames={{
              enterActive: s.burger__icon__entering,
              enterDone: s.burger__icon__entered,
              exitActive: s.burger__icon__exiting,
              exitDone: s.burger__icon__exited,
            }}
          >
            {sidebarIcon ? (
              <FaTimes
                onClick={isBarOpen ? closeSidebar : openSidebar}
                className={s.burger__icon}
              />
            ) : (
              <FaBars
                className={s.burger__icon}
                onClick={isBarOpen ? closeSidebar : openSidebar}
              />
            )}
          </CSSTransition>
          <Link to="/select-template">
            <Button title="Створити" className={s.create__btn} />
          </Link>
        </div>
        {isHomeScreen && (
          <div className={s.home__header__inner}>
            {/*<Link to="/select-template">*/}
            <Button size="lg" title="Створити" className={s.create__btn} />
            {/*</Link>*/}
            <img
              src={require("../../assets/home-header.png")}
              alt="loading"
              className={s.home__header__image}
            />
            <div className={s.home__header__main__content}>
              <h1 className={s.home__header__title}>
                {homeContent.header_1}
              </h1>
              <p className={s.home__header__desc}>
                {homeContent.header_text_1}
              </p>
            </div>
          </div>
        )}
      </div>

      <Menu
        width="300px"
        isOpen={isBarOpen}
        burgerButtonClassName={s.menu_hidden}
        menuClassName={s.menu_color}
        crossButtonClassName={s.exit_hidden}
        bmMenuWrap={s.menu_width}
        className={isBarOpen === null ? s.display_none : ""}
        disableAutoFocus
        itemListClassName={s.mobile__nav}
        itemClassName={s.mobile__nav__item}
        onStateChange={onStateMenuChange}
      >
        {/*<Link to="/" className={s.mobile__logo__container}>*/}
        {/*    <img*/}
        {/*        src={require("../../assets/logo.png")}*/}
        {/*        className={s.mobile__logo}*/}
        {/*        alt="logo"*/}
        {/*    />*/}
        {/*</Link>*/}
        <FaTimes className={s.close__icon__mobile} onClick={closeSidebar} />
        <Link to="/">  { menuContent.home} </Link>
        <Link to="/about-us">  { menuContent.about} </Link>
        <Link to="/tariffs">  { menuContent.tariffs} </Link>
        {!!user.id && <Link to="/sites"> { menuContent.sites} </Link>}
        {!!user.id && <Link to="/gallery">  { menuContent.gallery} </Link>}
        {user.id ? (
          <Link to="/profile">  { menuContent.profile} </Link>
        ) : (
          <Link to="/login">  { menuContent.login} </Link>
        )}
      </Menu>
    </>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
  content: state.content,
});

const mapDispatchToProps = (dispatch) => ({
  changeLanguage: (lang) => dispatch(changeLanguageAction(lang)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
