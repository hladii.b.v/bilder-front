import React from "react";
import s from "./PublicOffer.module.css";
import FixedWrapper from "../../wrappers/FixedWrapper/FixedWrapper";
import { connect } from "react-redux";
import { withRouter } from "react-router";

const PublicOffer = (content) => {

  const {
    page_content: { offer: offerContent },   
  } = content.content;

  return (
    <FixedWrapper className={s.section}>
      <h1>{offerContent.header}</h1>
      <br/>
      <p>
        {offerContent.main}
        
      </p>
      <hr />
      <div className={s.section}>
        <h3>{offerContent.header_1}</h3> <br />
        
          {offerContent.text_1_1}
        <br />
        
          {offerContent.text_1_2}
        <br />
      </div>      
      
      <div className={s.section}>
        <h3>{offerContent.header_2}</h3>
        <br /> 
          {offerContent.text_2_1}
        <br />
        {offerContent.text_2_2}
        <br />
        {offerContent.text_2_3}
        <br />
        {offerContent.text_2_4}
        <br />
        {offerContent.text_2_4_a}
        <br />
        {offerContent.text_2_4_b}
      </div>
      <div className={s.section}>
        <h3>{offerContent.header_3}</h3>
        <br />
        {offerContent.text_3_1}
        <br />
        {offerContent.text_3_2}
        <br />
        {offerContent.text_3_3}
        <br />
        {offerContent.text_3_4}
        <br />
        {offerContent.text_3_5}
        <br />
        {offerContent.text_3_6}
        <br />
        {offerContent.text_3_7}
        <br />
        {offerContent.text_3_8}
      </div>
      <div className={s.section}>
        <h3>{offerContent.header_4}</h3>
        <br />
        {offerContent.text_4_1}
        <br />
        {offerContent.text_4_2}
        <br />
        {offerContent.text_4_2_1}
        <br />
        {offerContent.text_4_2_2}
        <br />
        {offerContent.text_4_2_3}
        <br />
        {offerContent.text_4_2_4}
        <br />
        {offerContent.text_4_3}
        <br />
        {offerContent.text_4_4}
        <br />
        {offerContent.text_4_5}
        <br />
        {offerContent.text_4_6}
        <br />
        {offerContent.text_4_7}
        <br />
        {offerContent.text_4_8}
      </div>
      <div className={s.section}>
        <h3>{offerContent.header_5}</h3>
        <br />
        {offerContent.text_5_1}
        <br />
        {offerContent.text_5_2}
        <br />
        {offerContent.text_5_3}
        <br />
        {offerContent.text_5_4}
      </div>
      <div className={s.section}>
        <h3>{offerContent.header_6}</h3>
        <br />
        {offerContent.text_6_1}
        <br />
        {offerContent.text_6_2}
        <br />
        {offerContent.text_6_3}
        <br />
        {offerContent.text_6_4}
        <br />
        {offerContent.text_6_5}
        <br />
        {offerContent.text_6_6}
      </div>
      <div className={s.section}>
        <h3>{offerContent.header_7}</h3>
        <br />
        {offerContent.text_7_1}
        <br />
        {offerContent.text_7_2}
        <br />
        {offerContent.text_7_3}
        <br />
        {offerContent.text_7_4}
      </div>
    </FixedWrapper>
  );
};

const mapStateToProps = (state) => ({  
  content: state.content,
});


export default withRouter(connect(mapStateToProps, )(PublicOffer));
