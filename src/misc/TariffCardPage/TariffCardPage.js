import React from "react";
import s from "./TariffCardPage.module.css";
import classnames from "classnames";
import Button from "../Button/Button";
import CardWrapper from "../../wrappers/CardWrapper/CardWrapper";
import { Link } from "react-router-dom";
import { ReactComponent as AiOutlineLike } from "../../assets/like.svg";

const TariffCard = ({ isPrimary, tariff, selectText, month , month_6, month_12}) => {
  const { tariff_price: tariffPrices, tariff_text: tariffTexts } = tariff;
  return (
    <CardWrapper className={s.container}>
      
        <div
          className={classnames(s.header, { [s.header__primary]: isPrimary })}
        >
          <h4 className={s.title}>{tariff.name} </h4>
          <h3 className={s.price}>
            {tariffPrices[0].value} - {month}
          </h3>
          <h3 className={s.price}>
            {tariffPrices[1].value} - {month_6}
          </h3>
          <h3 className={s.price}>
            {tariffPrices[2].value} - {month_12}
          </h3>
          <div className={s.button__container}>
            <Link to="/sites">
              <Button className={s.button} title={selectText} />
            </Link>
          </div>
        </div>
        <div className={s.main__content}>
          {tariffTexts.map((text, i) => (
            <div className={s.desc} key={`tariff_text${i}`}>
              <p>
                 <AiOutlineLike className={s.icon} />{text.value}
              </p>
            </div>
          ))}
        </div>
      
      
    </CardWrapper>
  );
};

export default TariffCard;
