import React from "react";
import { withRouter } from "react-router";
import s from "./Footer.module.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

const Footer = (content) => {

  const {
    page_content: { home: homeContent,  menu: menuContent },   
  } = content.content;

 
  return (
    <div className={s.footer__container}>
      <div className={s.container}>
        <div className={s.row}>
          <div className={s.columnFirst}>
              <Link to="/">
                <img
                  src={require("../../assets/logo.png")}
                  className={s.logo}
                  alt="loading"
                />
              </Link>
              <p className={s.text}>
                {homeContent.footer_1}
              </p>
          </div>
          <div className={s.column}>
            <ul  className={s.list}>
              <li className={s.heading}>{menuContent.footer_header_1}</li>
              <li ><Link to="/public-offer" className={s.link_li}>{menuContent.footer_link_1_1}</Link></li>
              <li ><Link to="/about-us" className={s.link_li}>{menuContent.footer_link_1_2}</Link></li>           
            </ul>   
          </div>
          <div className={s.column}>
            <ul  className={s.list}>
              <li className={s.heading}>{menuContent.footer_header_2}</li>
              <li ><Link to="/public-offer" className={s.link_li}>{menuContent.footer_link_2_1}</Link></li>
              <li ><Link to="/about-us" className={s.link_li}>{menuContent.footer_link_2_2}</Link></li>           
            </ul> 
          </div>
          <div className={s.column}>
            <ul  className={s.list}>
              <li className={s.heading}>{menuContent.footer_header_3}</li>
              <li ><Link to="/public-offer" className={s.link_li}>{menuContent.footer_link_3_1}</Link></li>
              <li ><Link to="/about-us" className={s.link_li}>{menuContent.footer_link_3_2}</Link></li>
              
            </ul> 
          </div>
          <div className={s.column}>
            <ul  className={s.list}>
              <li className={s.heading}>{menuContent.footer_header_4}</li>                         
              <li ><Link to="/public-offer" className={s.link_li}>{menuContent.footer_offer}</Link></li>
              <li > <Link to="/terms" className={s.link_li}>{menuContent.footer_terms}</Link> </li>                          
            </ul> 
          </div>
        </div>
        <div className={s.rowSecond}>
            <div className={s.column}>
              <p className={s.link_li}>
                {homeContent.footer_2}
              </p>                      
            </div>           
        </div>
      </div>
    </div>
  );
};
const mapStateToProps = (state) => ({  
  content: state.content,
});



export default withRouter(connect(mapStateToProps, )(Footer));;
